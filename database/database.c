#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>
#include <ctype.h>
#define PORT 8080

int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char *db = "/home/charles/Desktop/Database/databases";
char *userDatabase = "/home/charles/Desktop/Database/databases/userDatabase/userDatabase.txt";
char currentDB[1024] = {0};
char currentUsername[1024] = {0};
int rootUser = 0;
int normalUser = 0;

void newConnection();
int authenticate(char* userAndPassword);
char* trimWhiteSpace(char* str);
void printBuffer(char *str);
char* create_user(char* buffer);
char* grant_permission(char buffer[]);
char* use_database(char buffer[]);
char* create_database(char buffer[]);


int main() 
{
      pid_t pid, sid;
      pid = fork();

      if (pid < 0) {
              exit(EXIT_FAILURE);
      }
      
      if (pid > 0) {
              exit(EXIT_SUCCESS);
      }
      
      umask(0);

      sid = setsid();
      if (sid < 0) {
              exit(EXIT_FAILURE);
      }

      if ((chdir("/")) < 0) {
          exit(EXIT_FAILURE);
      }
      
      close(STDIN_FILENO);
      close(STDOUT_FILENO);
      close(STDERR_FILENO);

      open("/dev/null", O_RDONLY);
      open("/dev/null", O_RDWR);
      open("/dev/null", O_RDWR);

      char buffer[1024] = {0};
	char msg[1024] = {0};

      if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
          perror("socket failed");
          exit(EXIT_FAILURE);
      }
          
      if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
          perror("setsockopt");
          exit(EXIT_FAILURE);
      }
          
      address.sin_family = AF_INET;
      address.sin_addr.s_addr = INADDR_ANY;
      address.sin_port = htons( PORT );
      
      if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
          perror("bind failed");
          exit(EXIT_FAILURE);
      }
          
      if (listen(server_fd, 3) < 0) {
          perror("listen");
          exit(EXIT_FAILURE);
      }
/*
      if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
      }
*/
	newConnection();
      while(1) 
      {
        bzero(buffer, 1024);
	bzero(msg, 1024);
        valread = read(new_socket, buffer, 1024);
        // kalo client 
        if(!valread)
        {
		rootUser = 0;
		normalUser = 0;
              newConnection();
              continue;
        }
        //send(new_socket, buffer, strlen(buffer), 0);


	if(buffer [strlen(buffer) - 1] != ';')
        {

            strcpy(msg, "SYNTAX ERROR!");
        }
        else if(!strncmp(buffer, "CREATE USER", 11))
        {
            if(rootUser)
            {
		//if rootUser boleh buat user
                strcpy(msg, create_user(buffer));
            }
            else
            {
		//if user biasa tidak bisa
                strcpy(msg, "COMMAND DENIED!");
            }
        }
        else if(!strncmp(buffer, "GRANT PERMISSION", 16))
        {
            if(rootUser)
            {
		//if rootUser boleh grant permission
                strcpy(msg, grant_permission(buffer));
            }
            else
            {
		//if user biasa tidak bisa
                strcpy(msg, "COMMAND DENIED");
            }
        }
        else if(!strncmp(buffer, "USE", 3))
        {
            strcpy(msg, use_database(buffer));
        }
	else if(!strncmp(buffer, "CREATE DATABASE", 15))
	{
		strcpy(msg, create_database(buffer));
	}
/*	else if(!strncmp(buffer, "DROP DATABASE", 13))
	{
		strcpy(msg, drop_database(buffer));
	}
	else if(!strncmp(buffer, "CREATE TABLE", 12))
	{
		strcpy(msg, create_table(buffer));
	}
        else
        {
            strcpy(msg, "ERROR : NO SUCH COMMAND KNOWN!");
        }*/
	send(new_socket, msg, strlen(msg), 0);
      }
      return 0;
}

void newConnection()
{
    	char buffer[1024] = {0};
	char msg[1024] = {0};
   	if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        	perror("accept");
        	exit(EXIT_FAILURE);
    	}

	//read username:password
    	valread = recv(new_socket, buffer, 1024, 0);

	if(!strcmp(buffer, "0"))
	{
		rootUser = 1;
		normalUser = 1;
		strcpy(msg, "Berhasil login sebagai root user!");
	}
	else
	{
		if(authenticate(buffer))
		{
			//send(new_socket, "Berhasil Autentikasi!", 40, 0);
			char* nama = strtok(buffer, ":");
			strcpy(msg, "Berhasil login sebagai ");
			strcat(msg, nama);
			strcat(msg, "!");
			strcpy(currentUsername, nama);
		}
		else
		{
			//send(new_socket, "Salah Autentikasi!", 40, 0);
			strcpy(msg, "Username atau password invalid!");
		}
	}
	send(new_socket, msg, strlen(msg), 0);
}


int authenticate(char* userAndPassword)
{
        FILE *input = fopen(userDatabase, "r");
        char temp[255];
        while(fgets(temp, 255, input))
        {
                char* trimmedLine = trimWhiteSpace(temp);
                //printf("line : %s a\n", trimmedLine);
                if(!strcmp(trimmedLine, userAndPassword))
                {
                        fclose(input);
                        return 1;
                }
        }
        fclose(input);
        return 0;
}

char* trimWhiteSpace(char* str)
{
        while (isspace((unsigned char) *str))
        {
                str++;
        }

        if(*str == '\0')
        {
                return str;
        }

        char* end = str + strlen(str) - 1;
        while(end > str && isspace((unsigned char) *end))
        {
                end--;
        }
        *(end + 1) = '\0';
        return str;
}

char* create_user(char* buffer)
{
        //printf("buffer %s\n", buffer);
        const char s[2] = " ";
        char *token;
        char *ptr;
        char msg[1024];
        char newUsername[300];
        char newPassword[300];
        /* get the first token */
	bzero(msg, 1024);
	bzero(newUsername, 300);
	bzero(newPassword, 300);
        token = strtok(buffer, s);

        int i = 0;
        /* walk through other tokens */
        while(token != NULL)
        {
                if(((i == 3) && strcmp(token, "IDENTIFIED")) || ((i == 4) && strcmp(token, "BY")) || i > 5)
                {
                        strcpy(msg, "SYNTAX ERROR");
                       ptr = msg;
                        return ptr;
                }
                if(i == 2)
                {
                        //printf("token saat username : %s\n", token);
                        strcpy(newUsername, token);
                }
                else if(i == 5)
                {
                        //printf("token after : %s\n", token);
                        strncpy(newPassword, token, strlen(token) - 1);
                }
                token = strtok(NULL, s);
                i++;
        }

        if(i != 6)
        {
                strcpy(msg, "SYNTAX ERROR");
                ptr = msg;
                return ptr;
        }
        char temp[1024];
	bzero(temp, 1024);
        sprintf(temp, "%s:%s", newUsername, newPassword);
        //printf("temp : %s\n", temp);
        FILE *output = fopen(userDatabase, "a");
        if(output == NULL)
        {
                printf("ERROR OPENING FILE\n");
        }
        fprintf(output, "%s\n", temp);
        fclose(output);

        strcpy(msg, "SUCCESFULY CREATED NEW USER!");
        ptr = msg;
        return ptr;
}

char* grant_permission(char buffer[])
{
        const char s[2] = " ";
        char *token;
        char *ptr;
        char msg[1024];
        char namaDB[200];
        char namaUser[200];
	bzero(msg, 1024);
	bzero(namaDB, 200);
	bzero(namaUser, 200);
        /* get the first token */
        token = strtok(buffer, s);
        /* walk through other tokens */
        int i = 0;
        while(token != NULL)
        {
                //printf("i : %d, token : %s\n", i, token);
                if(((i == 3) && strcmp(token, "INTO")) || i > 4)
                {
                        strcpy(msg, "SYNTAX ERROR");
                }
                if(i == 2)
                {
                        strcpy(namaDB, token);
                }
                if(i == 4)
                {
                        strncpy(namaUser, token, strlen(token) - 1);
                }
                token = strtok(NULL, s);
                i++;
        }

        if(i != 5)
        {
                strcpy(msg, "SYNTAX ERROR");
                ptr = msg;
                return ptr;
        }

	FILE *ip = fopen(userDatabase, "r");
	char str[1024];
	bzero(str, 1024);
	int found = 0;
	while(fgets(str, 1024, ip))
	{
		if(!strncmp(str, namaUser, strlen(namaUser)))
		{
			found = 1;
			break;
		}
	}
	fclose(ip);

	if(!found)
	{
		strcpy(msg, "THERE IS NO USER WITH THAT NAME!");
		ptr = msg;
		return ptr;
	}



        char pathToFile[400];
	bzero(pathToFile, 400);
        sprintf(pathToFile, "%s/%s/user_granted.txt", db, namaDB);
        FILE *input = fopen(pathToFile, "a");
        if(input == NULL)
        {
		strcpy(msg, "DATABASE NOT FOUND");
		ptr = msg;
                printf("Error Opening File!\n");
		return ptr;
        }
        fprintf(input, "%s\n", namaUser);
        fclose(input);
        strcpy(msg, "ACCESS GRANTED");
        ptr = msg;
        return ptr;
}

char* use_database(char buffer[])
{
	char *ptr;
	char msg[1024];

	bzero(msg, 1024);

	char* useptr = buffer + 4;
    	char namaDB[500];
    	bzero(namaDB, 500);
    	strncpy(namaDB, useptr, strlen(useptr) - 1);

    	char pathToFile[1024];
    	bzero(pathToFile, 1024);
    	sprintf(pathToFile, "%s/%s/user_granted.txt", db, namaDB);


	FILE *input = fopen(pathToFile, "r");
	if(input == NULL)
	{
		strcpy(msg, "DATABASE NOT FOUND");
		ptr = msg;
		return ptr;
	}

	char temp[1000];
        bzero(temp, 1000);
        sprintf(temp, "%s/%s", db, namaDB);

	if(rootUser)
	{
		strcpy(currentDB, temp);
		strcpy(msg, "CONNECTED TO DATABASE");
		ptr = msg;
		return ptr;
	}
	
	char str[1024];
	bzero(str, 1024);
	while(fgets(str, 1024, input))
	{
		if(!strncmp(str, currentUsername, strlen(currentUsername)))
                {
                        fclose(input);
			strcpy(msg, "CONNECTED TO DATABASE");
			ptr = msg;
			strcpy(currentDB, temp); 
                        return ptr;
                }
	}
	fclose(input);
	strcpy(msg, "YOU ARE NOT PERMITTED TO ACCES THIS DATABASE!");
	ptr = msg;
	return ptr;
}


char* create_database(char buffer[])
{
	const char s[2] = " ";
	char* ptr;
	char* token;
	char msg[1024];
	bzero(msg, 1024);

	char namaDB[500];
	bzero(namaDB, 500);

	/* get the first token */
        token = strtok(buffer, s);
        /* walk through other tokens */
        int i = 0;
        while(token != NULL)
        {
                if(i == 2)
                {
                        strncpy(namaDB, token, strlen(token)-1);
                }
		token =	strtok(NULL, s);
		i++;
        }

	if(i != 3)
	{
		strcpy(msg, "SYNTAX ERROR");
		ptr = msg;
		return ptr;
	}

	// printf("db : %s\n", namadb);

	char pathToDB[1024];
	bzero(pathToDB, 1024);
	sprintf(pathToDB, "%s/%s", db, namaDB);
	char* pathdatabase = pathToDB;

	// printf("%s\n", databasepath);

	if (mkdir(pathdatabase, 0777) != 0)
	{
		strcpy(msg, "CANNOT CREATE DATABASE!");
		ptr = msg;
		return ptr;
	}

	char user_granted_file[1024];
	bzero(user_granted_file, 1024);
	strcpy(user_granted_file, pathToDB);
	strcat(user_granted_file, "/user_granted.txt");

	FILE *input = fopen(user_granted_file, "w");
	fprintf(input, "%s\n", currentUsername);
	fclose(input);

	strcpy(msg, "DATABASE SUCCESSFULLY CREATED!");
	ptr = msg;
	return ptr;
}


