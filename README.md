# Sisop Praktikum FP 2023 AM B07

Pada praktikum ini kami hanya berhasil mengerjakan samapai bagian C (CREATE DATABASE) berikut adalah dokumentasi codingan kami.


Pertama kita dapat run `./database` yang akan berjalan sebagai daemon process.

![rundatabase](rundatabase.png)

Bisa kita cek menggunakan `ps -aux | grep database` bahwa process sudah menjadi daemon process.

![daemon](daemon.png)

Setelah itu bisa kita coba run `./client` dengan username dan password yang sudah ada di user database. Format user dan password adalah `user:password` seperti berikut :

![user](user.png)

Kita run `./client -u testing -p admin`, setelah itu kita create `database3` dan coba `USE` database tersebut :

![command](command.png)

Bisa kita lihat setelah kita create `database3` akan ada folder baru dengan nama `database3` :

![database3](database3.png)

Dan didalam granted user untuk database itu sudah ada nama user yang membuat database tersebut yakni `testing` :

![userdatabase3](userdatabase3.png)

Demikian praktikum yang dapat kami kerjakan.


