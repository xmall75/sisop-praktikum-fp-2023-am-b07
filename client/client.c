#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

char currentUser[255];

int main(int argc, char const *argv[])
{
	if(!strcmp(argv[0], "sudo"))
	{
		if(argc == 2)
		if(argc != 5)
        	{
                	printf("Jumlah Argumen Tidak Sesuai!\n");
                	printf("Format adalah sebagai berikut :\n");
                	printf("./client -u [username] -p [password]\n");
                	return 0;
        	}
	}
      struct sockaddr_in address;
      int sock = 0, valread;
      struct sockaddr_in serv_addr;
      char buffer[1024] = {0};

      if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
      {
            printf("\n Socket creation error \n");
            return -1;
      }

      memset(&serv_addr, '0', sizeof(serv_addr));

      serv_addr.sin_family = AF_INET;
      serv_addr.sin_port = htons(PORT);

      if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
      {
            printf("\nInvalid address/ Address not supported \n");
            return -1;
      }

      if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
      {
            printf("\nConnection Failed \n");
            return -1;
      }


	if(geteuid() == 0)
	{
		send(sock, "0", 1, 0);
		strcpy(currentUser, "root");
	}
	else
	{
		if((!strcmp(argv[1], "-u")) && (!strcmp(argv[3], "-p")))
		{
			bzero(buffer, 1024);
			sprintf(buffer, "%s:%s", argv[2], argv[4]);
			send(sock, buffer, strlen(buffer), 0);
			//printf("berhasil send %s\n", buffer);
			strcpy(currentUser, argv[2]);
		}
		else
		{
			//printf("%s %s %s %s\n", argv[1], argv[2], argv[3], argv[4]);
			printf("Command salah!\n");
			printf("Format adalah sebagai berikut :\n");
			printf("./client -u [username] -p [password]\n");
			return 0;
		}
	}

	bzero(buffer, 1024);
	read(sock, buffer, 1024);
	printf("%s\n", buffer);

	if(!strcmp(buffer, "Username atau password invalid!"))
	{
		return 0;
	}


	char input[255];
      while (1)
      {
            printf("[%s]: ", currentUser);
            scanf(" %[^\n]", input);
            send(sock, input, strlen(input), 0);

            bzero(buffer, 1024);
            read(sock, buffer, 1024);

            printf("%s\n", buffer);
      }

      return 0;
}
